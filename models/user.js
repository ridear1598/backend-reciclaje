const db=require('../config/config');
const crypto = require('crypto');
const User={};


/*Todo moviles */
User.getAll=()=>{
     const sql=`select * from users`;
     return db.manyOrNone(sql);
}

User.create=(user)=>{

     const myPasswordHash=crypto.createHash('md5').update(user.password).digest('hex');
     user.password=myPasswordHash;
     // const sql=`insert into users(email,name,lastname,phone,password,image,created_at,updated_at)
     // values($1,$2,$3,$4,$5,$6,$7,$8) returning id`;

     const sql=`UPDATE users set password=$2,updated_at=$3
     WHERE dni=$1`
     return db.none(sql,[
          user.dni,user.password,new Date()
     ])
     // return db.oneOrNone(sql,[
     //      user.email,
     //      user.name,
     //      user.lastname,
     //      user.phone,
     //      user.password,
     //      user.image,
     //      new Date(),
     //      new Date
     // ]);
}
User.findById=(id,callback)=>{
     const sql=`select id,email,name, "password",lastname,image,phone,session_token 
     from users where id=$1`;
     return db.oneOrNone(sql,id).then(user=>{callback(null,user);});
     
}

User.findByUserId=async (id)=>{
     const sql=`select u.id,email,u.name, password,u.lastname,u.image,u.phone,u.session_token,json_agg(
          json_build_object(
               'id',r.id,
               'name',r.name,
               'image',r.image,
               'route',r.route
          ) ) as roles from users as u inner join user_has_roles as uhr on uhr.id_user=u.id
           inner join roles as r on r.id= uhr.id_rol
           where u.id=$1
           GROUP BY u.id`;
      let result= await db.oneOrNone(sql,id);
      return result;
}

User.findByDNI=async (dni)=>{
     console.log('el dni es'+dni);
     const sql=`select u.id,email,u.name,u.dni, password,u.lastname,u.image,u.phone,u.session_token,json_agg(
          json_build_object(
               'id',r.id,
               'name',r.name,
               'image',r.image,
               'route',r.route
          ) ) as roles from users as u inner join user_has_roles as uhr on uhr.id_user=u.id
           inner join roles as r on r.id= uhr.id_rol
           where u.dni=$1
           GROUP BY u.id`;
      let result= await db.oneOrNone(sql,dni);
      return result;
}




User.findByRecicladorMen=async ()=>{
     const sql=`select u.id,email,u.name, password,u.lastname,u.image,u.phone,u.session_token from users as u
     inner join user_has_roles as uhr on uhr.id_user=u.id
     inner join roles as R on R.id=uhr.id_rol
	 where  R.id=3`;
      let result= await db.manyOrNone(sql);
      return result;
}

User.update=(user)=>{
     const sql=`UPDATE users set name=$2, lastname=$3, phone=$4, updated_at=$5
     WHERE id=$1`
     return db.none(sql,[
          user.id,user.name,user.lastname, user.phone, new Date()
     ])
}

User.isPasswordMatched=(userPassword,hash)=>{
     const myPasswordHash=crypto.createHash('md5').update(userPassword).digest('hex');
     if(myPasswordHash===hash){
          return true;
     }
     return false;
}

User.buscarIdPrueba= async (id)=>{
     const sql=`select * from users 
           where id=$1`;
      let result= await db.oneOrNone(sql,id);
      return result;
}

/* Todo web */
User.getAllContribuyentesAndRecicladores=async (id_rol)=>{
     const sql=`select * from users as u
     join user_has_roles as uhr on u.id=uhr.id_user 
     where id_rol=$1`;
      let result= await db.manyOrNone(sql,id_rol);
      return result;
}

User.createContribuyenteOrReciclador=(user)=>{
     const sql=`insert into users(email,name,lastname,phone,image,created_at,updated_at,dni)
     values($1,$2,$3,$4,$5,$6,$7,$8) returning id`;

     return db.oneOrNone(sql,[
          user.email,
          user.name,
          user.lastname,
          user.phone,
          user.image,
          new Date(),
          new Date,
          user.dni
     ]);
}


User.getByDNI= async (dni)=>{
     const sql=`select * from users
     where dni=$1`;
      let result= await db.oneOrNone(sql,dni);
      return result;
}
module.exports=User;
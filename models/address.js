const db = require('../config/config');

const Adress={}


Adress.findByIdUser=(id_user)=>{
const sql=`select id, id_user, address, neighbordhood,lat,lng from address where id_user=$1`;

return db.manyOrNone(sql,id_user)

}
Adress.create=(address) => {
    
    const sql =` INSERT INTO address(id_user,
        address,
        neighbordhood,
        lat,
        lng,
        created_at,
        updated_at) values($1,$2,$3,$4,$5,$6,$7) returning id`;

        return db.oneOrNone(sql,[
            address.id_user,
            address.address,
            address.neighbordhood,
            address.lat,
            address.lng,
            new Date(),
            new Date()])
}
module.exports =Adress;
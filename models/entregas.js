const db = require('../config/config');

const Entregas={}



Entregas.findByStatus=(status)=>{
    const sql =`select e.id, e.id_client, e.id_delivery, e.id_adress, e.status, e."timestamp",
    ehr.descripcion, ehr.quantity, ehr.tipo,
        JSON_BUILD_OBJECT(
            'id',u.id,
            'name', u.name,
            'lastname',u.lastname
        ) as client,

         JSON_BUILD_OBJECT(
            'id',u2.id,
            'name', u2.name,
            'lastname',u2.lastname
        ) as reciclador,

        JSON_BUILD_OBJECT(
            'id',a.id,
            'address', a.address,
            'neighbordhood',a.neighbordhood,
            'lat', a.lat,
            'lng', a.lng
        ) as address
        from entregas as e
        inner join users as u on e.id_client=u.id
        left join users as u2 on e.id_delivery=u2.id
        inner join address as a on a.id=e.id_adress
        INNER JOIN entregas_has_reciclaje AS ehr on ehr.id_entrega=e.id
        where e.status=$1`;

    return db.manyOrNone(sql,status)
}
Entregas.create=(entrega) => {
    
    const sql =` INSERT INTO entregas(id_client,
        id_adress,
        status,
        timestamp,
        created_at,
        updated_at) values($1,$2,$3,$4,$5,$6) returning id`;

        return db.oneOrNone(sql,[
            entrega.id_client,
            entrega.id_adress,
            entrega.status,
            Date.now(),
            new Date(),
            new Date()])
}

Entregas.update=(entrega) => {
    const SQL=`UPDATE entregas set id_client=$2, id_adress=$3, id_delivery=$4, status=$5, updated_at=$6 
    where id=$1`;

    return db.none(SQL,[entrega.id, 
        entrega.id_client,
         entrega.id_adress, 
         entrega.id_delivery, 
         entrega.status,
         new Date()])
}


Entregas.findByRecicladorAndStatus=(id_reciclador, status)=>{
    const sql =`select e.id, e.id_client, e.id_delivery, e.id_adress, e.status, e."timestamp",
    ehr.descripcion, ehr.quantity, ehr.tipo,
        JSON_BUILD_OBJECT(
            'id',u.id,
            'name', u.name,
            'lastname',u.lastname
        ) as client,

         JSON_BUILD_OBJECT(
            'id',u2.id,
            'name', u2.name,
            'lastname',u2.lastname
        ) as reciclador,

        JSON_BUILD_OBJECT(
            'id',a.id,
            'address', a.address,
            'neighbordhood',a.neighbordhood,
            'lat', a.lat,
            'lng', a.lng
        ) as address
        from entregas as e
        inner join users as u on e.id_client=u.id
        left join users as u2 on e.id_delivery=u2.id
        inner join address as a on a.id=e.id_adress
        INNER JOIN entregas_has_reciclaje AS ehr on ehr.id_entrega=e.id
        where e.id_delivery=$1 and e.status=$2 ` ;

    return db.manyOrNone(sql,[id_reciclador,status])
}

Entregas.findByContribuyenteAndStatus=(id_cliente, status)=>{
    const sql =`select e.id, e.id_client, e.id_delivery, e.id_adress, e.status, e."timestamp",
    ehr.descripcion, ehr.quantity, ehr.tipo,
        JSON_BUILD_OBJECT(
            'id',u.id,
            'name', u.name,
            'lastname',u.lastname
        ) as client,

         JSON_BUILD_OBJECT(
            'id',u2.id,
            'name', u2.name,
            'lastname',u2.lastname
        ) as reciclador,

        JSON_BUILD_OBJECT(
            'id',a.id,
            'address', a.address,
            'neighbordhood',a.neighbordhood,
            'lat', a.lat,
            'lng', a.lng
        ) as address
        from entregas as e
        inner join users as u on e.id_client=u.id
        left join users as u2 on e.id_delivery=u2.id
        inner join address as a on a.id=e.id_adress
        INNER JOIN entregas_has_reciclaje AS ehr on ehr.id_entrega=e.id
        where e.id_client=$1 and e.status=$2 ` ;

    return db.manyOrNone(sql,[id_cliente,status])
}

/*Todo Web*/
Entregas.findByStatusWeb=(status)=>{
    
    const sql =`select e.id, e.id_client, e.id_delivery, e.id_adress, e.status, e."timestamp",
    ehr.descripcion, ehr.quantity, ehr.tipo,
        JSON_BUILD_OBJECT(
            'id',u.id,
            'name', u.name,
            'lastname',u.lastname
        ) as client,

         JSON_BUILD_OBJECT(
            'id',u2.id,
            'name', u2.name,
            'lastname',u2.lastname
        ) as reciclador,

        JSON_BUILD_OBJECT(
            'id',a.id,
            'address', a.address,
            'neighbordhood',a.neighbordhood,
            'lat', a.lat,
            'lng', a.lng
        ) as address
        from entregas as e
        inner join users as u on e.id_client=u.id
        left join users as u2 on e.id_delivery=u2.id
        inner join address as a on a.id=e.id_adress
        INNER JOIN entregas_has_reciclaje AS ehr on ehr.id_entrega=e.id
        where e.status=$1`;

    return db.manyOrNone(sql,status)
}



Entregas.updateAsignadas=(entrega) => {
    const SQL=`UPDATE entregas set  id_delivery=$2, status=$3, updated_at=$4 
    where id=$1`;

    return db.none(SQL,[entrega.id, 
         entrega.id_delivery, 
         entrega.status,
         new Date()])
}
module.exports =Entregas;
const db = require('../config/config');

const EntregasHasReciclaje={}

EntregasHasReciclaje.create=(id_entrega, cantidad, tipo, descripcion) => {
    
    console.log('datos traidos',id_entrega, cantidad, tipo, descripcion);
    const sql =` INSERT INTO entregas_has_reciclaje(id_entrega,
        tipo,
        descripcion,
        quantity,
        created_at,
        updated_at) values($1,$2,$3,$4,$5,$6)`;

        return db.none(sql,[
            id_entrega,
            tipo,
            descripcion,
            cantidad,
            new Date(),
            new Date()])
}
module.exports =EntregasHasReciclaje;
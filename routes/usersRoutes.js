const UsersController= require('../controllers/usersControllers')

module.exports =(app)=>{

    /*Todo movil*/
    app.get('/api/users/getAll',UsersController.getAll);
    app.get('/api/users/findById/:id',UsersController.findById);
    app.put('/api/users/register',UsersController.register);
    app.put('/api/users/update',UsersController.update);
    app.post('/api/users/login',UsersController.login);
    /*Metodo de prueba*/
    app.get('/api/users/buscarId/:id',UsersController.findByIdPrueba);
    app.get('/api/users/findReciclador',UsersController.findReciladorMan);

    /*Todo Web*/
    app.get('/api/users/getAllContribuyentesAndRecicladores/:id_rol',UsersController.getAllContribuyentesAndRecicladores);
    app.post('/api/users/registerContribuyenteOrReciclador/:rol',UsersController.registerContribuyenteOrReciclador);
}
const AdressController= require('../controllers/adressController')

module.exports =(app)=>{

    app.post('/api/adress/create',AdressController.create);
    app.get('/api/adress/findByUser/:id_user',AdressController.findByuser);
}
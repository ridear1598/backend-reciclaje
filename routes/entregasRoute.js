const entregasController= require('../controllers/entregasController.js')

module.exports =(app)=>{

    app.post('/api/entregas/create',entregasController.create);

    app.get('/api/entregas/findByStatus/:status',entregasController.findByStatus);

    app.get('/api/entregas/findByRecicladorAndStatus/:id_reciclador/:status',entregasController.findByRecicladorAndStatus);

    app.get('/api/entregas/findByContribuyenteAndStatus/:id_cliente/:status',entregasController.findByContribuyenteAndStatus);

    app.put('/api/entregas/updateToEntrega',entregasController.updateToDispach);

    app.put('/api/entregas/updateToEncamino',entregasController.updateToEnCamino);

    app.put('/api/entregas/updateToEncaminoReciclador',entregasController.updateToEnCaminoReciclador);


    /* Todo web */
     app.get('/api/entregas/findByStatusWeb/:status',entregasController.findByStatusWeb);
     app.put('/api/entregas/updateToEntregaToAsignada',entregasController.updateToAsignada);

}


select e.id, e.id_client, e.id_delivery, e.id_adress, e.status, e."timestamp",
    ehr.descripcion, ehr.quantity, ehr.tipo,
        JSON_BUILD_OBJECT(
            'id',u.id,
            'name', u.name,
            'lastname',u.lastname
        ) as client,

         JSON_BUILD_OBJECT(
            'id',u2.id,
            'name', u2.name,
            'lastname',u2.lastname
        ) as reciclador,

        JSON_BUILD_OBJECT(
            'id',a.id,
            'address', a.address,
            'neighbordhood',a.neighbordhood,
            'lat', a.lat,
            'lng', a.lng
        ) as address
        from entregas as e
        inner join users as u on e.id_client=u.id
        inner join users as u2 on e.id_delivery=u2.id
        inner join address as a on a.id=e.id_adress
        INNER JOIN entregas_has_reciclaje AS ehr on ehr.id_entrega=e.id
        where e.status='EN CAMINO'



DROP TABLE IF EXISTS roles CASCADE;
CREATE TABLE roles (
	id BIGSERIAL PRIMARY KEY,
	name varchar(100) not null unique,
	image varchar(255) null,
	route varchar(255) null,
	created_at TIMESTAMP(0) not null,
	updated_at TIMESTAMP(0) not null
)

INSERT INTO roles(name,route,created_at,updated_at,updated_at) values ('CLIENTE','client/products/list','2021-07-12','2021-07-12')

DROP TABLE IF EXISTS roles CASCADE;
CREATE TABLE users(
	id BIGSERIAL PRIMARY KEY,
	email varchar(230) not null unique,
	name varchar(120) not null,
	password varchar(255) not null,
	lastname varchar(120) not null,
	phone varchar(80) not null unique,
	image varchar(255) null,
	is_available boolean null,
	session_token varchar(255) null,
	created_at TIMESTAMP(0) not null,
	updated_at TIMESTAMP(0) not null
);

DROP TABLE IF EXISTS  user_has_roles CASCADE;
CREATE TABLE user_has_roles(
	id_user BIGSERIAL not null,
	id_rol BIGSERIAL not null,
	created_at TIMESTAMP(0) not null,
	updated_at TIMESTAMP(0) not null,
	FOREIGN KEY (id_user) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (id_rol) REFERENCES roles(id) ON UPDATE CASCADE ON DELETE CASCADE,
	PRIMARY KEY(id_user,id_rol)
)


DROP TABLE IF EXISTS  address CASCADE;
CREATE TABLE address(
	id BIGSERIAL primary key,
	id_user Bigint not null,
	address VARCHAR(255) not null,
	neighbordhood VARCHAR(255) not null,
	lat decimal default 0,
	lng decimal default 0,
	created_at TIMESTAMP(0) not null,
	updated_at TIMESTAMP(0) not null,
	FOREIGN KEY (id_user) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE
)


DROP TABLE IF EXISTS  entregas CASCADE;
CREATE TABLE entregas(
	id BIGSERIAL primary key,
	id_client Bigint not null,
	id_delivery bigint not null,
	id_adress bigint not null,
	lat decimal default 0,
	lng decimal default 0,
	status  varchar(90) not null,
	timestamp BEGIN not null,
	created_at TIMESTAMP(0) not null,
	updated_at TIMESTAMP(0) not null,
	FOREIGN KEY (id_client) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (id_delivery) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (id_adress) REFERENCES address(id) ON UPDATE CASCADE ON DELETE CASCADE

);

DROP TABLE IF EXISTS  entregas_has_reciclaje CASCADE;
CREATE TABLE entregas_has_reciclaje(
	id_entrega BIGint NOT NULL,
	quantity bigint not null,
	created_at TIMESTAMP(0) not null,
	updated_at TIMESTAMP(0) not null,
	primary KEY(id_entrega),
	FOREIGN KEY (id_entrega) REFERENCES entregas(id) ON UPDATE CASCADE ON DELETE CASCADE

);
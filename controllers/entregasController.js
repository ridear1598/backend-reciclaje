const Entregas= require('../models/entregas');
const EntregasHasReciclaje= require('../models/entregas_has_reciclaje');

  
    module.exports = {
        async create(req, res, next){
            try {
                const entrega =req.body;
                
                const data =await Entregas.create(entrega);

                await EntregasHasReciclaje.create(data.id, entrega.cantidad, entrega.tipo, entrega.descripcion);
                return res.status(201).json({
                    success:true,
                    msg:'La entrega a sido creada',
                    data:data.id,
            });

            } catch (error) {
                console.log('Error es', error); 
                return res.status(501).json({
                        success:false,
                        msg:'Hubo un error creando entrega',
                        error:error
                });
            }
        },

        async findByStatus(req, res, next){
            try {
                const status = req.params.status;
                const data= await Entregas.findByStatus(status);
                return res.status(201).json(data);
            }catch(err){
                console.log(`Error:${err}`);
                return res.status(501).json({
                    success:false,
                    msg:'Error al obtener las entregas'
                });
            }
        },


        async findByRecicladorAndStatus(req, res, next){
            try {
                const status = req.params.status;
                const id_reciclador = req.params.id_reciclador;
                console.log('los datos son',id_reciclador, status);
                const data= await Entregas.findByRecicladorAndStatus(id_reciclador, status);
                console.log(data);
                return res.status(201).json(data);
            }catch(err){
                console.log(`Error:${err}`);
                return res.status(501).json({
                    success:false,
                    msg:'Error al obtener las entregas'
                });
            }
        },


        async findByContribuyenteAndStatus(req, res, next){
            try {
                const status = req.params.status;
                const id_cliente = req.params.id_cliente;
                const data= await Entregas.findByContribuyenteAndStatus(id_cliente, status);
                return res.status(201).json(data);
            }catch(err){
                console.log(`Error:${err}`);
                return res.status(501).json({
                    success:false,
                    msg:'Error al obtener las entregas'
                });
            }
        },

        async updateToDispach(req, res, next){
            try {
                const entrega =req.body;
                entrega.status='EN CAMINO'
                const data =await Entregas.update(entrega);
                return res.status(201).json({
                    success:true,
                    msg:'La entrega a sido actualizada'
            });

            } catch (error) {
                console.log('Error es', error); 
                return res.status(501).json({
                        success:false,
                        msg:'Hubo un error al actualizar orden',
                        error:error
                });
            }
        },


        async updateToEnCamino(req, res, next){
            try {
                const entrega =req.body;
                entrega.status='EN CAMINO'
                // entrega.status='PENDIENTES'
                const data =await Entregas.update(entrega);
                return res.status(201).json({
                    success:true,
                    msg:'La entrega a sido actualizada'
            });

            } catch (error) {
                console.log('Error es', error); 
                return res.status(501).json({
                        success:false,
                        msg:'Hubo un error al actualizar orden',
                        error:error
                });
            }
        },


        async updateToEnCaminoReciclador(req, res, next){
            try {
                const entrega =req.body;
                entrega.status='RECOGIDAS'
                //entrega.status='PENDIENTES'
                const data =await Entregas.update(entrega);
                return res.status(201).json({
                    success:true,
                    msg:'La entrega a sido actualizada'
            });

            } catch (error) {
                console.log('Error es', error); 
                return res.status(501).json({
                        success:false,
                        msg:'Hubo un error al actualizar orden',
                        error:error
                });
            }
        },

        
        /*Todo web */


        async findByStatusWeb(req, res, next){
            try {
                const status = req.params.status;
                const data= await Entregas.findByStatusWeb(status);
                return res.status(201).json(data);
            }catch(err){
                console.log(`Error:${err}`);
                return res.status(501).json({
                    success:false,
                    msg:'Error al obtener las entregas'
                });
            }
        },
        

        async updateToAsignada(req, res, next){
            try {
                const entrega =req.body;
                console.log(entrega);
                entrega.status='ASIGNADAS'
                const data =await Entregas.updateAsignadas(entrega);
                return res.status(201).json({
                    success:true,
                    msg:'La entrega a sido actualizada'
            });

            } catch (error) {
                console.log('Error es', error); 
                return res.status(501).json({
                        success:false,
                        msg:'Hubo un error al actualizar orden',
                        error:error
                });
            }
        },

    }
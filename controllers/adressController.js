const Adress= require('../models/address');

  
    module.exports = {
        async create(req, res, next){
            try {
                const adress =req.body;
                
                const data =await Adress.create(adress);

                return res.status(201).json({
                    success:true,
                    msg:'La direccion se creo correctamente',
                    data:data.id,
            });

            } catch (error) {
                console.log('Error es', error); 
                return res.status(501).json({
                        success:false,
                        msg:'Hubo un error creando direccion',
                        error:error
                });
            }
        },


        async findByuser(req, res, next){
            try {
                const id_user = req.params.id_user;

                const data= await Adress.findByIdUser(id_user);
                return res.status(201).json(data);
            }catch(err){
                console.log(`Error:${err}`);
                return res.status(501).json({
                    success:false,
                    msg:'Error al obtener las direcciones'
                });
            }
        },
    
    }

const User=require('../models/user');
const jwt=require('jsonwebtoken');
const keys =require('../config/keys');
const Rol=require('../models/rol');

module.exports ={
    async getAll(req, res, next){
        try {
            const data= await User.getAll();
            return res.status(201).json(data);
        }catch(err){
            console.log(`Error:${err}`);
            return res.status(501).json({
                success:false,
                msg:'Error al obtener los usuarios'
            });
        }
    },


    async findById(req, res, next){
        try {
            const id= req.params.id;
            const data= await User.findByUserId(id);
            return res.status(201).json(data);
        }catch(err){
            console.log(`Error:${err}`);
            return res.status(501).json({
                success:false,
                msg:'Error al obtener el usuario'
            });
        }
    },

    async findReciladorMan(req, res, next){
        try {
            const data= await User.findByRecicladorMen();
            return res.status(201).json( data);
        }catch(err){
            console.log(`Error:${err}`);
            return res.status(501).json({
                success:false,
                msg:'Error al obtener el recilador'
            });
        }
    },


    async login(req, res, next){
        try {
            const dni=req.body.dni;
            const password=req.body.password;
            const MyUser=await User.findByDNI(dni);
            if(!MyUser){
                return res.status(401).json({
                    success:false,
                    msg:'El usuario no existe'
                });
            }
            if(User.isPasswordMatched(password,MyUser.password)){
                const token= jwt.sign({id:MyUser.id,dni:MyUser.dni},keys.secretOrKey,{
                    //expiresIn:(60*60*24)
                });
                const data={
                    id:MyUser.id,
                    name:MyUser.name,
                    dni:MyUser.dni,
                    lastname:MyUser.lastname,
                    email:MyUser.email,
                    phone:MyUser.phone,
                    roles:MyUser.roles,
                    session_token:`JWT ${token}`
                }
                 console.log(`USER ENVIADO ${data}`);
                return res.status(201).json({
                    success:true,
                    data:data,
                    msg:'el usuaario a sido autenticado'
                })
            }else{
                return res.status(401).json({
                    success:false,
                    msg:'La contraseña es incorrecta'
            });
        }
        

        } catch (error) {
            console.log(`Error ${error}`);
            return res.status(501).json({
                success:false,
                msg:'Error al momento del login',
                error:error
            });
        }
    },

    async register(req, res, next){
        try {
            const user=req.body;

            const MyUser=await User.getByDNI(user.dni);
            if(!MyUser){
                return res.status(401).json({
                    success:false,
                    msg:'No pertenece a la asociación'
                });
            }
            const data= await User.create(user);
            //    await Rol.create(data.id,1);//Rol  por defecto cliente
            return res.status(201).json({
                success:true,
                msg:'El registro se realizo correctamente, ahora inicia sesion',
                data:data
            });
        }catch(err){
            console.log(`Error:${err}`);
            return res.status(501).json({
                success:false,
                msg:'Error al registrar error'
            });
        }
    },

    async update(req, res, next){
        try {
            const user=req.body;
            await User.update(user);

            return res.status(201).json({
                success:true,
                msg:'El usuario se actualizo correctamente',
            });
        }catch(err){
            console.log(`Error:${err}`);
            return res.status(501).json({
                success:false,
                msg:'Error al actualizar'
            });
        }
    },

    async findByIdPrueba(req, res, next){
        try {
            const id= req.params.id;
            const data= await User.buscarIdPrueba(id);
            return res.status(201).json(data);
        }catch(err){
            console.log(`Error:${err}`);
            return res.status(501).json({
                success:false,
                msg:'Error al obtener el usuario'
            });
        }
    },

    /*Todo web */
    async getAllContribuyentesAndRecicladores(req, res, next){
        try {
            let id_rol=req.params.id_rol;
            const data= await User.getAllContribuyentesAndRecicladores(id_rol);
            return res.status(201).json( data);
        }catch(err){
            console.log(`Error:${err}`);
            return res.status(501).json({
                success:false,
                msg:'Error al obtener el Contribuyentes'
            });
        }
    },


    
    async registerContribuyenteOrReciclador(req, res, next){
        try {
            const rol=req.params.rol;
            const user=req.body;
            console.log(user);
            console.log(rol);
            const data= await User.createContribuyenteOrReciclador(user);

              await Rol.create(data.id,rol);//Rol  por defecto cliente

            return res.status(201).json({
                success:true,
                msg:'El registro se realizo correctamente, ahora inicia sesion',
                data:data.id
            });
        }catch(err){
            console.log(`Error:${err}`);
            return res.status(501).json({
                success:false,
                msg:'Error al registrar error'
            });
        }
    },
}
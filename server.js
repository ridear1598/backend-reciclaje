const express= require('express');
const app = express();
const http = require('http');
const server = http.createServer(app);
const logger=require('morgan');
const cors=require('cors');
const dotenv = require("dotenv");


/*
Rutas
*/ 
const users= require('./routes/usersRoutes')
const adress =require('./routes/adreesRoute')
const entregas =require('./routes/entregasRoute')

dotenv.config({ path: "./config/config.env" });

const port=process.env.PORT || 3500;
app.use(logger('dev'));
app.use(express.json())
app.use(express.urlencoded({
    extended:true,
}));
app.use(cors());
app.disable('x-powered-by');
app.set('port',port);

/*llamamos a las rutas*/ 
users(app);
adress(app);
entregas(app);

server.listen(port, function(){
   console.log('Corriendo en puerto: ' +process.env.NODE_ENV);
});

module.exports={
    app: app,
    server: server
}
//ERROR
app.use((err,req,res,next)=>{
    console.log(err);
    res.status(err.status||500).send(err.stack);
})